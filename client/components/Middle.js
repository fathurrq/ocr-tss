import React, { useState } from 'react'
import { Line } from 'react-chartjs-2';
import { Upload, message, Table } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import RightBar from './RightBar';
import axios from 'axios'
import useInterval from 'use-interval'

// const dataSource = [
//     {
//         key: '1',
//         count: 1,
//         container_code: 'MW CX 500-620',
//         timestamp: Date.now(),
//     }
// ];

const columns = [
    {
        title: 'Count',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Serial Number',
        dataIndex: 'serial_number',
        key: 'serial_number',
    },
    {
        title: 'Date',
        dataIndex: 'date',
        key: 'date',
    },
    {
        title: 'Captured Number',
        dataIndex: 'image_path',
        key: 'image_path',
        render: ((text, record, index) => (
            <img src={`/${text}`}/>
        ) )
    }
];

const Middle = () => {
    const { Dragger } = Upload;
    const [dropdown, setDropdown] = useState(false)
    const [active, setActive] = useState(0)
    let [data, setData] = useState([])
    // useEffect(() => {
    //     axios.get('http://localhost:5000/getdata').then((e) => {setData(e.data)
    // console.log(e.data)
    // }).catch((e) => console.log(e, "eroor"))
    // },[])
    useInterval(() => {
        axios.get('http://localhost:5000/getdata').then((e) => {
            setData(e.data)
        }).catch((e) => console.log(e, "eroor"))
      }, 3000); // passing null instead of 1000 will cancel the interval if it is already running
    
    
    const props = {
        name: 'file',
        multiple: true,
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        onChange(info) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
        onDrop(e) {
            console.log('Dropped files', e.dataTransfer.files);
        },
    };



    return (
        <div className=" bg-transparent ml-2   shadow-sm xl:w-full lg:w-full rounded-xl">
            <div className="flex space-x-3 items-center  ">
                {/* <div class="relative inline-block text-left p-4 w-full">
                    <div>
                        <button onClick={() => setDropdown(!dropdown)} type="button" class="inline-flex rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-transparent text-sm font-medium text-gray-300 hover:bg-transparent focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500" id="menu-button" aria-expanded="true" aria-haspopup="true">
                            {(active == 0 && 'Live Cam') || (active == 1 && 'Upload Video')}
                            <svg class="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            </svg>
                        </button>
                    </div>
                    {
                        dropdown &&
                        <div class="z-50 absolute left-0 m-4 w-56 rounded-md shadow-lg bg-white ring-1 ring-white ring-opacity-5 outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                            <div class="py-1" role="none">
                                <a onClick={() => {
                                    setActive(0)
                                    setDropdown(false)
                                }} href="#" class="text-gray-600 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-0">Live Cam</a>
                                <a onClick={() => {
                                    setActive(1)
                                    setDropdown(false)
                                }} href="#" class="text-gray-600 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-0">Upload Video</a>

                            </div>
                        </div>
                    }
                </div> */}
            </div>
            <div className="flex">
                <div className="flex flex-col items-center xl:w-full">
                    {/* {
                        active == 0 ?
                            <div className="pt-4 mb-8">
                                <img src="/gif.gif" className=" xl:h-auto lg:h-auto px-4 w-full" />
                            </div> :
                            <div className="p-4">
                                <Dragger {...props} style={{ outline: "none", border: "none", background: "#fafafa", borderRadius: 10, padding: 4}}>
                                    <p className="ant-upload-drag-icon">
                                        <InboxOutlined />
                                    </p>
                                    <p className="ant-upload-text">Click or drag file to this area to upload</p>
                                    <p className="ant-upload-hint">
                                        Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                                        band files
                                    </p>
                                </Dragger>
                            </div>
                    } */}
                    <div style={{height: 300}}>
                    <Table sortDirections={['descend']} pagination={false} bordered dataSource={data || []} columns={columns} scroll={{y: 350}} style={{ padding: "30px", backgroundColor: "transparent", background: "transparent" }} />;
                    </div>

                </div>
                <RightBar serial_number={data[data.length - 1]?.serial_number} date={data[data.length - 1]?.date} size={"45ft"} />
            </div>
        </div>
    )
}

export default Middle
