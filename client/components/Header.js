import React, { useContext, useState } from 'react'
import SessionContext from '../helper/SessionContext';

const Header = () => {
    const { data, setData } = useContext(SessionContext)
    const [dropdown, setDropdown] = useState(false)
    const [active, setActive] = useState(0)
    return (
        <div className="flex shadow-sm bg-dark  p-4 justify-between">
            <div className="flex space-x-3 items-center  ">
                <p className="text-gray-300">Videotron Location: </p>
                <div class="relative inline-block text-left">
                    <div>
                        <button onClick={() => setDropdown(!dropdown)} type="button" class="inline-flex w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-transparent text-sm font-medium text-gray-300 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500" id="menu-button" aria-expanded="true" aria-haspopup="true">
                            {data[active].location}
                            <svg class="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            </svg>
                        </button>
                    </div>
                    {
                        dropdown &&
                        <div class="origin-top-right absolute right-0 w-56 rounded-md shadow-lg bg-transparent ring-1 ring-black ring-opacity-5 outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                            <div class="py-1" role="none">
                                {
                                    data.map((d) => (
                                        <a onClick={() => {
                                            setActive(d.id)
                                            setDropdown(false)
                                        }} href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-0">{d.location}</a>
                                    ))
                                }
                            </div>
                        </div>
                    }
                </div>
            </div>

        </div>
    )
}

export default Header
