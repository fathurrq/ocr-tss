import React, { useEffect, useState } from 'react'
import Card from './Card'
import Middle from './Middle'
import RightBar from './RightBar'
import ArrowUpIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownIcon from '@material-ui/icons/ArrowDownward';

const Container = () => {
    return (
        <div className=" bg-gradient-to-r bg-dark h-auto " >
            {/* <div className="flex p-4 space-x-3">
                <Card title="TODAY" date={today.toDateString()} icon={4} value={todayValue} />
                <Card title="YESTERDAY" date={yesterday.toDateString()} icon={0} value={yesterdayValue} />
                <Card title="Percentage" date={(todayValue > yesterdayValue) ? getPercentageChange(yesterdayValue, todayValue) : `-${getPercentageChange(yesterdayValue, todayValue)}`} icon={(todayValue > yesterdayValue) ? 1 : 2} growth={true} value={(todayValue > yesterdayValue) ? <ArrowUpIcon /> : <ArrowDownIcon />} />
            </div> */}
            <div className="flex  ml-3 mt-6  mr-4">
                <Middle />
                {/* <RightBar /> */}
            </div>
        </div>
    )
}

export default Container
