module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#461baa',
        'dark-blue': '#118ce3',
        'light-blue': '#00ccf1',
        'light-purple' : '#c397d7',
        'dark-purple' : '#5a00b5',
        dark: '#1e1e2f'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
