import {NextResponse} from 'next/server'

const BASE_URL = 'http://localhost:3000'
export default function middleware(req) {
    const {token} = req.cookies
    const url = req.url
    if (!token && url !=`${BASE_URL}/login`){
        return NextResponse.redirect('/login')
    }
}
