import '../styles/globals.css'
import "tailwindcss/tailwind.css";
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'

import { SessionProvider } from '../helper/SessionContext';

function MyApp({ Component, pageProps }) {
  return (
    <SessionProvider>
      <Component {...pageProps} />
    </SessionProvider>
  )
}

export default MyApp
