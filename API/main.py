import pymysql
from app import app
from config import mysql
from flask import jsonify
from flask import flash, request

# run dari file sokin
@app.route('/', methods=['GET'])
def home():
    return "<h1>PLERRRR</h1>"

@app.route('/getdata')
def get_data():
    try:
        print("kepanggil getdata")
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT * FROM tss.ocr;")
        ocrRows = cursor.fetchall()
        respone = jsonify(ocrRows)
        respone.status_code = 200
        return respone
    except Exception as e :
        print(e, "***errornya get data")
    finally:
        cursor.close()
        conn.close()

@app.route('/search')
def search():
    try:
        print("kepanggil search")
        # input serial number
        sn = request.args 
        print(sn, "apa ini toed")
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT * FROM tss.ocr WHERE serial_number =%s;", sn)
        searchedRows = cursor.fetchall()
        respone = jsonify(searchedRows)
        respone.status_code = 200
        return respone
    except Exception as e:
        print(e, "***errornya search")
    finally:
        cursor.close()
        conn.close()

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Record not found: ' + request.url,
    }
    respone = jsonify(message)
    respone.status_code = 404
    return respone

if __name__ == "__main__":
    app.run()
